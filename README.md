# Role Pytorch - Torchvision - Pytorch Geometric

Ansible role to install silently Pytorch - Torchvision - Pytorch Geometric.

Compatibility
------------

This role can be used on Ubuntu

## Author Information

Written by [Dimitri Colier](mailto:dimitri.colier@epfl.ch) for EPFL - STI school of engineering